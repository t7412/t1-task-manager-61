package ru.t1.chubarov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.dto.request.DataBackupSaveRequest;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.event.ConsoleEvent;

@Component
public final class DataBackupSaveListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "backup-save";
    @NotNull
    private final String DESCRIPTION = "Save data backup to file.";

    @SneakyThrows
    @Override
    @EventListener(condition = "@dataBackupSaveListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final DataBackupSaveRequest request = new DataBackupSaveRequest(getToken());
        domainEndpoint.saveDataBackup(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
