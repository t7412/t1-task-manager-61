package ru.t1.chubarov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.dto.request.ProjectTaskBindToProjectRequest;
import ru.t1.chubarov.tm.event.ConsoleEvent;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.util.TerminalUtil;

@Component
public final class ProjectTaskBindToProjectListener extends AbstractProjectListener {

    @Override
    @EventListener(condition = "@projectTaskBindToProjectListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[BIND TASK TO PROJECT");
        System.out.println("ENTER PROJECT ID: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID: ");
        @NotNull final String taskId = TerminalUtil.nextLine();

        @NotNull final ProjectTaskBindToProjectRequest request = new ProjectTaskBindToProjectRequest(getToken());
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        projectEndpoint.bindTaskToProject(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-bind-to-project";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Bind task to project bi id.";
    }

}
