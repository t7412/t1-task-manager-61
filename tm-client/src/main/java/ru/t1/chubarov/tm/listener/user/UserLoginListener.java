package ru.t1.chubarov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.dto.request.UserLoginRequest;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.event.ConsoleEvent;
import ru.t1.chubarov.tm.util.TerminalUtil;

@Component
public final class UserLoginListener extends AbstractUserListener {

    @NotNull
    private final String NAME = "login";
    @NotNull
    private final String DESCRIPTION = "User login.";

    @Override
    @EventListener(condition = "@userLoginListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER LOGIN]");
        System.out.println("[ENTER LOGIN]");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD]");
        @NotNull final String password = TerminalUtil.nextLine();

        @NotNull final UserLoginRequest request = new UserLoginRequest(login, password);
        request.setLogin(login);
        request.setPassword(password);
        @Nullable final String token = authEndpoint.login(request).getToken();
        setToken(token);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
