package ru.t1.chubarov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.dto.request.ProjectChangeStatusByIdRequest;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.event.ConsoleEvent;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.util.TerminalUtil;

@Component
public final class ProjectCompleteByIdListener extends AbstractProjectListener {

    @Override
    @EventListener(condition = "@projectCompleteByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[COMPLETE PROJECT BY ID");
        System.out.println("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final Status status = Status.toStatus("COMPLETED");
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken());
        request.setId(id);
        request.setStatus(status);
        projectEndpoint.changeProjectStatusById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-complete-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update project status to [Completed] by id.";
    }

}
