package ru.t1.chubarov.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}
