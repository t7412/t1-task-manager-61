package ru.t1.chubarov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.t1.chubarov.tm.model.Session;

import java.util.List;

@Repository
@Scope("prototype")
public interface ISessionModelRepository extends IModelRepository<Session> {

    @NotNull
    @Query("SELECT p FROM Session p ")
    List<Session> findAll();

    @Nullable
    List<Session> findAllByUserId(@Nullable String userId);

    @Nullable
    Session findFirstById(@NotNull String id);

    @Nullable
    Session findFirstByIdAndUserId (@Nullable String userId, @Nullable String id);

    void deleteAll();

    void deleteByUserId(@Nullable String userId);

    void deleteById(@Nullable String Id);

    void deleteByUserIdAndId(@Nullable String userId, @Nullable String id);

    long count();

    long countAllByUserId(@Nullable String userId);

    @NotNull
    Boolean existsByUserIdAndId(@Nullable String userId, @Nullable String id);

}
