package ru.t1.chubarov.tm.api.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.chubarov.tm.dto.model.AbstractUserOwnerModelDTO;

@Repository
@Scope("prototype")
public interface IUserOwnerDtoRepository<M extends AbstractUserOwnerModelDTO> extends IDtoRepository<M> {

}
