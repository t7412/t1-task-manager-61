package ru.t1.chubarov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.dto.request.UserLoginRequest;
import ru.t1.chubarov.tm.dto.request.UserLogoutRequest;
import ru.t1.chubarov.tm.dto.request.UserProfileRequest;
import ru.t1.chubarov.tm.dto.response.UserLoginResponse;
import ru.t1.chubarov.tm.dto.response.UserLogoutResponse;
import ru.t1.chubarov.tm.dto.response.UserProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAuthEndpoint extends IEndpoint {

    @NotNull
    String NAME = "AuthEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IAuthEndpoint.class);
    }

    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IAuthEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserLoginResponse login(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserLoginRequest request);

    @NotNull
    @WebMethod
    UserLogoutResponse logout(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserLogoutRequest request);

    @NotNull
    @WebMethod
    UserProfileResponse profile(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserProfileRequest request);

}
