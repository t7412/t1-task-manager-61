package ru.t1.chubarov.tm.api;

//import com.hazelcast.core.MessageListener;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jms.MessageListener;

public interface IReceiverService {

    @SneakyThrows
    void receive(@NotNull @Autowired MessageListener listener);

}
